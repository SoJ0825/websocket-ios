//
//  ViewController.swift
//  app
//
//  Created by soj on 2021/9/10.
//

import PusherSwift
import UIKit

class ViewController: UIViewController, PusherDelegate  {
    
    var pusher: Pusher! = nil
    let decoder = JSONDecoder()
    
    @IBOutlet weak var publicChannelMessage: UILabel!
    @IBOutlet weak var privateChannelMessage: UILabel!
    
    @IBAction func subscribePublicChannelButton(_ sender: Any) {
        let myChannel = pusher.subscribe("new-message")
        
        _ = myChannel.bind(eventName: "App\\Events\\NewMessage", eventCallback: { (event: PusherEvent) in

            guard let json: String = event.data,
                  let jsonData: Data = json.data(using: .utf8)
            else {
                print("Could not convert JSON string to data")
                return
            }
            let decodedMessage = try? self.decoder.decode(DebugConsoleMessage.self, from: jsonData)
            guard let message = decodedMessage else {
                print("Could not decode message")
                return
            }
            self.publicChannelMessage.text = message.message
            //            print("\(message.name) says \(message.message)")
        })
    }
    @IBAction func subscribePrivateChannelButton(_ sender: Any) {
        // subscribe to a channel
        let myChannel = pusher.subscribe("private-new-message")
        
        // bind a callback to event "my-event" on that channel
        _ = myChannel.bind(eventName: "App\\Events\\PrivateMessage", eventCallback: { (event: PusherEvent) in
            print("get event")
            // convert the data string to type data for decoding
            guard let json: String = event.data,
                  let jsonData: Data = json.data(using: .utf8)
            else {
                print("Could not convert JSON string to data")
                return
            }
            // decode the event data as json into a DebugConsoleMessage
            let decodedMessage = try? self.decoder.decode(DebugConsoleMessage.self, from: jsonData)
            guard let message = decodedMessage else {
                print("Could not decode message")
                return
            }
            self.privateChannelMessage.text = message.message
            //            print("\(message.name) says \(message.message)")
        })
    }
    
    
    @IBAction func  disconnectButton(_ sender: Any) {
        pusher.disconnect()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let optionsWithEndpoint = PusherClientOptions(
            authMethod: AuthMethod.authRequestBuilder(authRequestBuilder: AuthRequestBuilder()),
            host: .host("ws.soj.tw"),
            port: 443,
            path: "/ios",
            useTLS: true
        )
        pusher = Pusher(key: "local", options: optionsWithEndpoint)
        pusher.delegate = self
        pusher.connect()
    }
    
    // PusherDelegate methods
    func changedConnectionState(from old: ConnectionState, to new: ConnectionState) {
        // print the old and new connection states
        print("old: \(old.stringValue()) -> new: \(new.stringValue())")
    }
    
    func subscribedToChannel(name: String) {
        print("Subscribed to \(name)")
    }
    
    func debugLog(message: String) {
        print(message)
    }
    
    func receivedError(error: PusherError) {
        if let code = error.code {
            print("Received error: (\(code)) \(error.message)")
        } else {
            print("Received error: \(error.message)")
        }
    }
}

class AuthRequestBuilder: AuthRequestBuilderProtocol {
    func requestFor(socketID: String, channelName: String) -> URLRequest? {
        var request = URLRequest(url: URL(string: "https://ws.soj.tw/broadcasting/auth")!)
        request.httpMethod = "POST"
        request.httpBody = "socket_id=\(socketID)&channel_name=\(channelName)".data(using: String.Encoding.utf8)
        request.addValue("Bearer 1|4rgReWg4fYdbfhPBLCw8p8ksSyPQYTH4ywyewKw2", forHTTPHeaderField: "Authorization")
        return request
    }
}

struct DebugConsoleMessage: Codable {
    //    let name: String
    let message: String
}
